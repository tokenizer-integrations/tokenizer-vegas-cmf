<?php
/**
 * This file is part of Vegas package
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TokenizerVegasCmf\Controllers\Frontend;
use Auth\Models\BaseUser;

/**
 * Class AuthController
 *
 * @ACL(name='mvc:auth:Frontend\Auth', description='Authentication')
 * @package TokenizerVegasCmf\Controllers\Frontend
 */
class AuthController extends \Vegas\Mvc\Controller\ControllerAbstract
{

    /**
     * @ACL(name='login', description='Login action')
     * @throws \Vegas\Security\Authentication\Exception\InvalidCredentialException
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function loginAction()
    {

        if ($this->di->get('auth')->isAuthenticated()) {
            return $this->response->redirect('');
        }
        $this->view->setLayout('login');
        $this->view->setRenderLevel(\Vegas\Mvc\View::LEVEL_LAYOUT);

        if ($this->request->isPost()) {
            try {
                $email = $this->request->getPost('email');
                $password = $this->request->getPost('password');

                $this->serviceManager->getService('tokenizerVegasCmf:auth')->login($email, $password);

                $this->serviceManager->getService('tokenizerVegasCmf:auth')->loginTokenizer($email);

                $redirectUrl = '';
                if ($this->session->has('redirect_url')) {
                    $redirectUrl = ltrim($this->session->get('redirect_url'), '/');
                }

                $this->session->remove('redirect_url');
                return $this->response->redirect($redirectUrl);
            } catch (\Vegas\Security\Authentication\Exception $ex) {
                $this->flash->error($this->i18n->_($ex->getMessage()));
            }
        }
    }

}
 
