<?php
/**
 * This file is part of Vegas package
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace TokenizerVegasCmf\Controllers\Backend;

use Auth\Models\BaseUser;
use TokenizerVegasCmf\Services\Tokenizer;
use Vegas\Mvc\Controller\ControllerAbstract;
use Vegas\Security\Authentication\Identity;

/**
 * Class TokenizerController
 * @package TokenizerVegasCmf\Controllers\Backend
 */
class TokenizerController extends ControllerAbstract
{
    public function tokenizeAction($id)
    {
        $sessionManager = $this->di->get('sessionManager');
        try {

            if(!$id) {
                throw new \AmsterdamStandard\Tokenizer\Exception($this->i18n->_('No url id found'));
            }

            $tokenizer = new Tokenizer([
                'app_id'  => APP_ID,
                'app_key' => APP_KEY,
            ]);
            if(!$sessionManager->scopeExists('tokenizer')) {
                $tokenizerScope = $sessionManager->createScope('tokenizer');
            } else {
                $tokenizerScope = $sessionManager->getScope('tokenizer');
            }

            $tokenizer->setSessionStorage($tokenizerScope);
            if($tokenizer->getSession('tokenizer_id') === null) {
                throw new \AmsterdamStandard\Tokenizer\Exception($this->i18n->_('No tokenizer session found'));
            }

            if($tokenizer->getSession('tokenizer_id') != $id) {
                throw new \AmsterdamStandard\Tokenizer\Exception($this->i18n->_('Tokenizer session does not match url id'));
            }

            if( $tokenizer->verifyAuth($id) ) {
                $user = BaseUser::findFirst(array('conditions' => array('email' => $tokenizerScope->get('email'))));

                if($user) {
                    $identity = new Identity($user->getAttributes());
                    $scope = $sessionManager->createScope('auth');
                    $scope->set('identity', $identity);
                    $scope->set('authenticated', true);

                    $this->flash->success($this->i18n->_('Your authentication has been accepted'));
                } else {
                    throw new \AmsterdamStandard\Tokenizer\Exception($this->i18n->_('Identity not found'));
                }

            } else {
                throw new \AmsterdamStandard\Tokenizer\Exception($this->i18n->_('Your authentication has not been accepted'));
            }

            $this->view->disable();
            $this->response->redirect();

        } catch(\AmsterdamStandard\Tokenizer\Exception $e) {
            $this->flash->error($e->getMessage());
            $sessionManager->getScope('tokenizer')->destroy();
            $this->view->disable();
            $this->response->redirect();
        }


    }
} 