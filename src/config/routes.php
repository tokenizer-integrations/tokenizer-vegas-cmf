<?php
return array(
    'tokenize' => array(
        'route' => '/tokenize/{id}',
        'paths' => array(
            'module'    =>  'TokenizerVegasCmf',
            'controller' => 'Backend\Tokenizer',
            'action' => 'tokenize',
            'id' => 1,
            'auth'  =>  false
        )
    ),

    'tokenizer_login' => array(
        'route' => '/tokenizer/login',
        'paths' => array(
            'module'    =>  'TokenizerVegasCmf',
            'controller' => 'Frontend\Auth',
            'action' => 'login',

            'auth'  =>  false
        )
    ),
);