<?php
/**
 * This file is part of Vegas package
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard Sp. Z o.o.
 * @homepage http://vegas-cmf.github.io
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace TokenizerVegasCmf\Services;

use User\Models\User;

class Auth implements \Phalcon\DI\InjectionAwareInterface
{
    use \Vegas\DI\InjectionAwareTrait;

    public function loginTokenizer($email)
    {
        $tokenizer = new \TokenizerVegasCmf\Services\Tokenizer([
            'app_id'  => APP_ID,
            'app_key' => APP_KEY,
        ]);
        $scope = $this->di->get('sessionManager')->createScope('tokenizer');
        $scope->set('email', $email);
        $tokenizer->setSessionStorage($scope);
        $tokenizer->createAuth($email, APP_URL, $redirect = true);

    }

    public function login($email, $password)
    {
        $user = User::findFirst(array(array('email' => $email)));
        if (!$user) {
            throw new \Vegas\Security\Authentication\Exception\InvalidCredentialException();
        }
        $this->di->get('auth')->authenticate($user, $password);

        // clear scope in case of success authorization
        $this->clearAuthScope();
    }

    public function logout() 
    {
        $auth = $this->di->get('auth');
        $auth->logout();
    }

    public function clearAuthScope()
    {
        $this->di->get('auth')->logout();
    }
}
